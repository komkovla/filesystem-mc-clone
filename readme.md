Správce souborů
===============

Program na správu souborů/adresářů pod unix-based systemy. Program se
otevira na domacim adresaři uživatele, odkud uživatel muze prohližet a
provadet ruzne operace s soubory.

Funkcionalita
=============

1.  vytváření a mazání souborů/adresářů/symbolických linků
2.  kopírování a přesun souborů/adresářů/symbolických linků
3.  možnost provádět předchozí operace nad množinou souborů definovaných
    -   regulárním výrazem

4.  navigace mezi jednotlivými adresáři seřazení podlě:
    -   jmena
    -   rozměrů souborů

Navrch polymorfismu
===================

Soubor
------

Třida CAbstractFile má zakladní atributy a metody, ktere budou mít
stejny všíchní typy soubory (*Directory, file, sym\_link*) - attributy -
jmeno - cesta - rozměr - prava - metody -print

Operace
-------

trida COperation má zakladní atributy a (virtualní) metody, ktere budou
stejny pro všíchní operace vcetni navigacni operaci (*copy, paste, move,
delete, open, close* )

-   metody
    -   proved_operaci()

Filtrovaní
----------

trida CSort má zakladní atributy a (virtualní) metody, ktere budou
stejny pro každy typ filtrovaní(*by\_size, by\_extention, by\_regex*) -
attributy

-   metody
    -   sort()

Ui
--

trida CInterface má zakladní atributy a (virtualní) metody, ktere budou
stejny pro každy typ UI (*terminal, GUI*) 
metody - print()
