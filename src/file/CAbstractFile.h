#pragma once


#include <string>
#include <filesystem>
#include "../UI/CInterface.h"

namespace fs = std::filesystem;
/**
 * Abstract file class stores basic information about the file that have each type of file
 * information stored for all types: name, location, size, permissions
 */
class CAbstractFile {
public:
    /**
     * Constructor creates file from 'path' and fill up its path name and permition information
     * @param path
     */
    explicit CAbstractFile(const fs::path & path);



    /**
     * Get path
     * @return file path
     */
    const fs::path &getPath() const;
    /**
     * copies file
     * @return ptr on new copy of file
     */
    virtual std::unique_ptr<CAbstractFile> clone() = 0;
    /**
     * get Name of the file
     * @return file name
     */
     virtual void create(const std::string &fileName, std::unique_ptr<CInterface> &interface, CFileGroup &group) = 0;
    const std::string &getName() const;
    /**
     * get TypeName of the file
     * @return type of the file
     */
    const std::string &getTypeName() const;

    /**
     * Overloading << operator to print 'file' to interface
     * @param out
     * @param file
     * @return ptr of the 'out' stream
     */
    friend std::ostream & operator <<(std::ostream & out, const CAbstractFile & file);
    /**
     * Get size of the file
     * @return file size
     */
    virtual const size_t getSize() const;
    /**
     * print file to 'out' stream
     * @param out
     */
    virtual std::ostream & print(std::ostream &out) const = 0;
protected:
    std::string typeName;
    size_t size = 0;
    fs::path path;
    fs::perms perms;
    std::string name;
};



