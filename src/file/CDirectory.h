//
// Created by komko on 08.04.2021.
//

#pragma once


#include "CAbstractFile.h"
/**
 * Class specifies CAbstractfile info for Directory
 */
class CDirectory : public CAbstractFile {
public:
    explicit CDirectory(const fs::path & path) : CAbstractFile(path) {
    };

    std::ostream & print(std::ostream &out) const override;
    std::unique_ptr<CAbstractFile> clone() override;
    void create(const std::string &fileName, std::unique_ptr<CInterface> &interface, CFileGroup &group) override;

private:
};



