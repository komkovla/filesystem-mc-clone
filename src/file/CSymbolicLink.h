//
// Created by komko on 08.04.2021.
//

#pragma once


#include "CAbstractFile.h"
/**
 * Class specifies CAbstractfile info for Symbolic link
 *
 */
class CSymbolicLink : public CAbstractFile{
public:
    /**
     * Completes all CAbstractFile info along with the reference path information for symLink in the 'path'
     * @param path
     * @param reference
     */
    explicit CSymbolicLink(const fs::path & path) : CAbstractFile(path) {}
    //~CSymbolicLink() override = default;
    virtual std::ostream & print(std::ostream &out) const;
    std::unique_ptr<CAbstractFile> clone() override;
    void create(const std::string &fileName, std::unique_ptr<CInterface> &interface, CFileGroup &group) override;


private:
};



