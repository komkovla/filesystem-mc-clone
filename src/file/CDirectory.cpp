
#include <iostream>
#include "CDirectory.h"

std::ostream & CDirectory::print(std::ostream &out) const {
    return out << name << '/';
}

std::unique_ptr<CAbstractFile> CDirectory::clone() {
    return std::make_unique<CDirectory>(*this);
}

void CDirectory::create(const std::string &fileName, std::unique_ptr<CInterface> &interface, CFileGroup &group) {
    fs::create_directories(group.getPath() / fileName);
    group.add(std::make_pair(CDirectory(group.getPath() / fileName).clone(),false));
}
