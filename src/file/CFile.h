//
// Created by komko on 08.04.2021.
//

#pragma once


#include "CAbstractFile.h"
#include "../fileGroup/CFileGroup.h"
/**
 * Class specifies CAbstractfile info for regular file
 * - add info about size
 */
class CFile : public CAbstractFile{
public:
    /**
     * Completes all CAbstractFile info along with the size information for file in the 'path'
     * @param path
     */
    explicit CFile(const fs::path & path);

    std::ostream & print(std::ostream &out) const override;
    std::unique_ptr<CAbstractFile> clone() override;
    void create(const std::string &fileName, std::unique_ptr<CInterface> &interface, CFileGroup &group) override;
    const size_t getSize() const override;

private:
};



