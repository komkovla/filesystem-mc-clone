
#include <fstream>
#include "CFile.h"
#include <iomanip>

CFile::CFile(const fs::path & path) : CAbstractFile(path) {
}

void printSize(size_t fileSize, std::ostream & out){
    std::array<std::string, 4> sizeArr = {"B", "KiB", "MiB", "GiB"};
    size_t index = 0;
    size_t leftOver = 0;
    while(fileSize > 1024 && index < sizeArr.size()){
        index++;
        leftOver = fileSize % 1024;
        fileSize /= 1024;
    }
    out <<std::fixed << std::setprecision(2)<< (float)fileSize + (float)leftOver/1024 << sizeArr[index];
}

std::ostream & CFile::print(std::ostream &out) const{
    size_t fileSize = file_size(path);
    out << name << "( ";
    printSize(fileSize, out);
    out << " )";
    return out;

}

std::unique_ptr<CAbstractFile> CFile::clone() {
    return std::make_unique<CFile>(*this);
}

void CFile::create(const std::string &fileName, std::unique_ptr<CInterface> &interface, CFileGroup &group) {
    std::ofstream fileStream(absolute(group.getPath()) / fileName);
    fileStream.close();
    group.add(std::make_pair(CFile(group.getPath() / fileName).clone(),false));
}

const size_t CFile::getSize() const {
    return file_size(path);
}
