
#include "CAbstractFile.h"

    CAbstractFile::CAbstractFile(const fs::path & path) : path(path) {
        name = path.filename().c_str();
        perms = status(path).permissions();
    }

    const fs::path & CAbstractFile::getPath() const {
        return path;
    }

    const std::string & CAbstractFile::getName() const {
        return name;
    }

std::ostream & operator <<(std::ostream & out, const CAbstractFile & file){
    return file.print(out);
    }

    const size_t CAbstractFile::getSize() const {
        return size;
    }

const std::string &CAbstractFile::getTypeName() const {
    return typeName;
}



