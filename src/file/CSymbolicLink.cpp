
#include "CSymbolicLink.h"

std::ostream & CSymbolicLink::print(std::ostream &out) const{
    return out << name << " -> " << fs::read_symlink(path).lexically_normal().c_str();
}

std::unique_ptr<CAbstractFile> CSymbolicLink::clone() {
    return std::make_unique<CSymbolicLink>(*this);
}

void CSymbolicLink::create(const std::string &fileName, std::unique_ptr<CInterface> &interface, CFileGroup &group) {
    std::pair<fs::path, bool> linkTo = interface ->choosePath(group);
    if(linkTo.second){
        fs::create_symlink(linkTo.first, group.getPath() / fileName);
        group.add(std::make_pair(CSymbolicLink(group.getPath() / fileName).clone(),false));
    }
}
