
#pragma once


#include "COperation.h"
/**
 * Delete operation
 */
struct CDelete : public COperation{
    CDelete(std::string && name): COperation(std::move(name)) {};
    /**
     * Deletes files selected from 'group ' by user in 'interface'
     * @param group
     * @param interface
     * @param buffer
     * @return always true
     */
    bool process(CFileGroup &group, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) override;
};



