#include "CCopy.h"


bool CCopy::process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer)
{
    if(interface ->fileOperationMenu(files)){
        for (auto it = files.getFiles().begin(); it != files.getFiles().end(); it++) {
            if (it->second) {
                buffer.add(files.clonePair(*it));
            }
        }
    }
    return true;

}


