#pragma once
#include "COperation.h"

/**
 * Move file operation
 */
struct CMove : public COperation{
    CMove(std::string && name): COperation(std::move(name)) {};
    /**
     * Move file(s) from group to distanation chosen by usen in 'interface'
     * @param files
     * @param interface
     * @param buffer
     * @return always true
     */
    virtual bool process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer);
};



