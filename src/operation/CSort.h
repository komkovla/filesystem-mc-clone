#pragma once


#include <map>
#include "COperation.h"
#include "../fileGroup/CAbstractSort.h"
/**
 * Sorting operation
 */
class CSort : public COperation{
public:

    /**
     * sets map of sorting metods
     * @param name
     */
    CSort(std::string && name);
    /**
     * Sorts 'files' by method chosen in 'interface'
     * @param files
     * @param interface
     * @param buffer
     * @return always true
     */
    bool process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) override;

    /**
     * setting up all sorts variants
     */
    void setSortMap();
private:
    std::map<int, std::unique_ptr<CAbstractSort>> sortBy;
};