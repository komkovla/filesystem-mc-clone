//
// Created by ubuntu on 6/3/21.
//
#include "COperation.h"
#include "../UI/CInterface.h"
#include "CList.h"

bool CList::process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) {
    interface -> print(files);
    return true;
}

