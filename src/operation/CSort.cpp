
#include "CSort.h"
#include "../UI/CInterface.h"
#include "../fileGroup/CSortName.h"
#include "../fileGroup/CSortSize.h"

bool CSort::process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) {
    int index = interface->sortMenu(sortBy);
    if(index)
        sortBy[index] ->process(files);
    return true;
}

void CSort::setSortMap() {
    sortBy[1] = std::make_unique<CSortName>("by name");
    sortBy[2] = std::make_unique<CSortSize>("by size");
}
CSort::CSort(std::string && name) : COperation(std::move(name)){
    setSortMap();
}
