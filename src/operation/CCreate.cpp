#include "CCreate.h"
#include "../file/CDirectory.h"
#include "../file/CFile.h"
#include "../file/CSymbolicLink.h"

CCreate::CCreate(std::string && name): COperation(std::move(name)) { setFileTypes();}

bool CCreate::process(CFileGroup &group, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) {
    int index = interface->chooseFileType(group, fileTypes);
    if(index){
        std::string fileName = interface ->readFileName(group);
        if(!fileName.empty()){
            fileTypes[index]->create(fileName, interface, group);
        }
    }
    return true;
}

void CCreate::setFileTypes() {
    fileTypes[1] = std::make_unique<CDirectory>("Directory");
    fileTypes[2] = std::make_unique<CFile>("Ordinary File");
    fileTypes[3] = std::make_unique<CSymbolicLink>("Symbolic Link");

}
