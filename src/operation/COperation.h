
#pragma once


#include "../fileGroup/CFileGroup.h"
#include "../UI/CInterface.h"
#include "../file/CAbstractFile.h"
/**
 * Parent abstract class of any operation
 */
class COperation {
public:
    /**
     * Fill out 'name' of the opiration
     * @param name
     */
    COperation(std::string && name) : name(std::move(name)) {};
    /**
     * Destructs operation
     */
    virtual ~COperation() = default;
    /**
     * Process the operation through on a 'group', getting parametrs from user in 'interface', can buffer group for storing files
     * @param files
     * @param interface
     * @param buffer
     * @return false - on exit program, true - otherwise
     */
    virtual bool process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) = 0;

    /**
     * Get Name of the operation
     * @return operation name
     */
    virtual const std::string & getName() {return name;}

private:
    std::string name;
};



