

#include <iostream>
#include "COpen.h"
#include "../UI/CInterface.h"

bool COpen::process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) {
    int index = interface->chooseFileMenu(files);
    if (index == -1) return true;
    files.setPath((files.getFiles()).at(index).first->getPath(), interface);
    return true;
}

