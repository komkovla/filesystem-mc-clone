//
// Created by komko on 16.04.2021.
//

#pragma once


#include "COperation.h"
#include "../fileGroup/CFileGroup.h"
/**
 * Close folder operation
 */
class CClose : public COperation{
public:
    CClose(std::string && name): COperation(std::move(name)) {};
    /**
     * changes path of 'group' to one folder up the fs tree (/..)
     * @param files
     * @param interface
     * @param buffer
     * @return always true
     */
    bool process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) override;

private:
};




