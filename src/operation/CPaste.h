#pragma once


#include "COperation.h"

/**
 * Paste operation
 */
struct CPaste : public COperation{
    CPaste(std::string && name): COperation(std::move(name)) {};
    /**
     * Pastes files from 'buffer' to 'files' location
     * @param files
     * @param interface
     * @param buffer
     * @return always true
     */
    bool process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) override;
};



