#pragma once

#include "COperation.h"

/**
 * Copy operation
 */
struct CCopy : public COperation{
    CCopy(std::string && name): COperation(std::move(name)) {};
    /**
     * copies files from 'group' selected by user in 'interface' to 'buffer'
     * @param group
     * @param interface
     * @param buffer
     * @return always true
     */
    bool process(CFileGroup &group, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) override;

public:
};
