#include "CDelete.h"
#include "../UI/CInterface.h"
#include <vector>

bool CDelete::process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) {
    if(interface ->fileOperationMenu(files)) {
        for (auto it = files.getFiles().begin(); it != files.getFiles().end();) {
            if (it->second) {
                fs::remove_all(it->first->getPath());
                it = files.getFiles().erase(it);
            } else it++;
        }
    }
    return true;
}

