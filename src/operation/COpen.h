#pragma once

#include "COperation.h"
#include "../fileGroup/CFileGroup.h"
#include <filesystem>
namespace fs = std::filesystem;
/**
 * Open folder operation
 */
struct COpen : public COperation{
    COpen(std::string && name): COperation(std::move(name)) {};
    /**
     * Open folder chosen in 'interface'
     * trows filesystem exception on non-folder files, or folders without permission
     * @param files
     * @param interface
     * @param buffer
     * @return always true
     */
    virtual bool process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer);

};
