#pragma once

#include "COperation.h"

class CCreate: public COperation{
public:
    CCreate(std::string && name);
    /**
     * create file type chosen by the user
     * @param group - where the file will be created
     * @param interface - user interface
     * @param buffer - unused
     * @return always true
     */
    bool process(CFileGroup &group, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) override;
private:
    void setFileTypes();
    std::map<int, std::unique_ptr<CAbstractFile>> fileTypes;
};