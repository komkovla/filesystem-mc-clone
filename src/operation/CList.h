#pragma once


/**
 * Print files list operation
 */
struct CList : public COperation {
    CList(std::string && name): COperation(std::move(name)) {};
    /**
     * All files from 'group' printed out to the 'interface'
     * @param files
     * @param interface
     * @param buffer
     * @return always true
     */
    bool process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) override;

};
