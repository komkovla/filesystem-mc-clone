#pragma once

/**
 * Exit program operation
 */
struct CExit : public COperation {
    CExit(std::string && name): COperation(std::move(name)) {};
    /**
     * return false to exit program
     * @param files
     * @param interface
     * @param buffer
     * @return ALWAYS FALSE
     */
    bool process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) override;
};

