//
// Created by komko on 08.04.2021.
//

#include "CMove.h"
#include "../UI/CInterface.h"
#include "CPaste.h"

bool CMove::process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) {
     if(interface ->fileOperationMenu(files)) {
         std::pair<fs::path, bool> moveTo = interface->choosePath(files);
         if(moveTo.second){
             for (auto it = files.getFiles().begin(); it != files.getFiles().end(); ) {
                 if (it->second) {
                     fs::rename(it->first->getPath(), moveTo.first / it->first->getPath().filename().c_str());
                     it = files.getFiles().erase(it);
                 }
                 else it++;
             }
         }
     }
    return true;
}