#include "CPaste.h"

bool CPaste::process(CFileGroup &files, std::unique_ptr<CInterface> &interface, CFileGroup &buffer) {
    const auto copyOptions = fs::copy_options::recursive |
                             fs::copy_options::update_existing;
    if(buffer){
        for(auto & it : buffer.getFiles()){
           fs::copy(it.first->getPath(), files.getPath(), copyOptions);
           files.getFiles().emplace_back(buffer.clonePair(it));
        }
    }
    return true;
}

