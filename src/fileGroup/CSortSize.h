#pragma once


#include <memory>
#include "CAbstractSort.h"

struct CSortSize : public CAbstractSort{
    CSortSize(std::string && name): CAbstractSort(std::move(name)){};
    /**
     * Sorts 'files' by size descending
     * @param files
     */
    void process(CFileGroup &files) override;
};



