#include <iostream>
#include "CFileGroup.h"
#include "../file/CFile.h"
#include "../file/CDirectory.h"
#include "../file/CSymbolicLink.h"
#include "../UI/CInterface.h"
#include "../file/CAbstractFile.h"
#include "CSortSize.h"
/**
 * get distinct file, directory, or symbolic links from the file 'path'
 * @param path
 * @return ptr on file
 */
std::unique_ptr<CAbstractFile> fileFromPath( fs::path path){
    switch (status(path).type()) {
        case fs::file_type::regular:
            if(fs::is_symlink(path))
                return std::make_unique<CSymbolicLink>(path);
            return std::make_unique<CFile>(path);
        case fs::file_type::directory:
            if(fs::is_symlink(path))
                return std::make_unique<CSymbolicLink>(path);
            return std::make_unique<CDirectory>(path);
        default:
            return nullptr;
    }
}
/**
 * Iterates through the directory on 'path' and pushes back all the files on 'output' list
 * @param path
 * @param output
 */
void getFilesFromPath(const fs::path & path, std::vector<std::pair<std::unique_ptr<CAbstractFile>, bool>> & output){
    for(auto & it : fs::directory_iterator(path)){
        if(fileFromPath(it.path()))
            output.emplace_back(std::make_pair(fileFromPath(it.path()), false));
    }
}

CFileGroup::CFileGroup(const fs::path &path): path(path) {
    getFilesFromPath(path, files);
}

const std::vector<std::pair<std::unique_ptr<CAbstractFile>, bool>> & CFileGroup::getFiles() const {
    return files;
}
std::vector<std::pair<std::unique_ptr<CAbstractFile>, bool>> & CFileGroup::getFiles(){
    return files;
}

const fs::path &CFileGroup::getPath() const {
    return path;
}

bool CFileGroup::setPath(const fs::path &newPath, const std::unique_ptr<CInterface> &interface) {

    std::vector<std::pair<std::unique_ptr<CAbstractFile>, bool>> newFiles;
    try {
        getFilesFromPath(newPath, newFiles);
    }
    catch (std::exception & e){
        interface ->printException(e);
        return false;
    }
    CFileGroup::path = newPath;
    files.clear();
    files = std::move(newFiles);
    newFiles.clear();
    return true;
}

CFileGroup::CFileGroup(const std::unique_ptr<CAbstractFile> & file) {
    files.emplace_back(std::make_pair(file->clone(), false));
}

void CFileGroup::clearSelect() {
    for(auto & it : files){
        it.second = false;
    }
}
std::pair<std::unique_ptr<CAbstractFile>, bool> CFileGroup::clonePair(const std::pair<std::unique_ptr<CAbstractFile>, bool> &pair) const {
    return std::make_pair(pair.first->clone(), pair.second);
}

void CFileGroup::add(std::pair<std::unique_ptr<CAbstractFile>, bool> pair) {
    files.emplace_back(clonePair(pair));
}

CFileGroup::operator bool() const {
    return !files.empty();
}

CFileGroup::CFileGroup(const CFileGroup &src) {
    path = src.path;
    for(auto & it : src.files){
        files.emplace_back(src.clonePair(it));
    }
}
