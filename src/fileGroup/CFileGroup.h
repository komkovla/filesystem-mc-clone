#pragma once
#include <memory>
#include <vector>
#include <filesystem>
namespace fs = std::filesystem;
// forward declaration
class CAbstractFile;
struct CInterface;

class CFileGroup {
public:
    /**
     * Creates a Group of all files (being a directory symbolic link or ordinary) located in the 'path' directory
     * @param path
     */
    explicit CFileGroup(const fs::path & path);
    /**
     * Creates a file group of one single 'file'
     * @param file
     */
    explicit CFileGroup(const std::unique_ptr<CAbstractFile> & file);
    /**
     * Copy contructor
     * @param src - source group
     */
    CFileGroup(const CFileGroup & src);
    /**
     * Implicit constructor creates an empty group
     */
    CFileGroup() = default;
    /**
     * destructs group
     */
    ~CFileGroup() = default;
    /**
     * Get files from the const group
     * @return const list of files
     */
    const std::vector<std::pair<std::unique_ptr<CAbstractFile>, bool>> & getFiles() const;
    /**
     * Get files from the group
     * @return list of files
     */
    std::vector<std::pair<std::unique_ptr<CAbstractFile>, bool>> & getFiles();
    /**
     * Clears select mark on files
     * sets bool in pair<file, bool> on true for all files
     */
    void clearSelect();
    /**
     * Get path of the file group;
     * @return
     */
    const fs::path &getPath() const;
    /**
     * Changes current path of the group to the 'newPath', updating the files list
     * @param newPath
     * @param interface
     * @return true - success, false - on failure
     */
    bool setPath(const fs::path &newPath, const std::unique_ptr<CInterface> &interface);
    /**
     * add file to the group
     * @param pair
     */
    void add(std::pair<std::unique_ptr<CAbstractFile>, bool> pair);
    /**
     * bool conversion
     * @return false - empty group, true - consists at least one file
     */
    explicit operator bool() const;
    /**
     * copy of 'pair'
     * @param pair
     * @return duplicate 'pair'
     */
    std::pair<std::unique_ptr<CAbstractFile>, bool> clonePair(const std::pair<std::unique_ptr<CAbstractFile>, bool> &pair) const;
private:
    std::vector<std::pair<std::unique_ptr<CAbstractFile>, bool>> files;
    fs::path path;
};



