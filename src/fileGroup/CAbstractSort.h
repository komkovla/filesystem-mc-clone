#pragma once


#include <memory>
#include "../file/CAbstractFile.h"
#include "CFileGroup.h"
/**
 * Abstract class sort to sort files in the group
 */
class CAbstractSort {
public:
    /**
     * Creates abstract class with name
     * @param name - sort by
     */
    CAbstractSort(std::string && name) : name(std::move(name)){};
    virtual ~CAbstractSort() = default;
    /**
     * Proccess sort on a 'group'
     * @param files
     */
    virtual void process(CFileGroup &files) = 0;
    /**
     * Copies abstract sort
     * @return ptr on copied abstract sort
     */
    const std::string &getName() const {return name;}
protected:
    std::string name;
};



