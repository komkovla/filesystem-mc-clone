#pragma once


#include <memory>
#include "CAbstractSort.h"

struct CSortName : public CAbstractSort{
    CSortName(std::string && name): CAbstractSort(std::move(name)){};
    /**
     * Sorts 'files' by name alphabetically
     * @param files
     */
    void process(CFileGroup &files) override;
};



