#include <algorithm>
#include "CSortSize.h"


void CSortSize::process(CFileGroup &files) {
    std::sort(files.getFiles().begin(), files.getFiles().end(), [](const std::pair<std::unique_ptr<CAbstractFile>, bool> & f1, const std::pair<std::unique_ptr<CAbstractFile>, bool> & f2)
              {
                  return f1.first->getSize() > f2.first->getSize();
              }
            );
}

