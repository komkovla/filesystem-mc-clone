#include <algorithm>
#include "CSortName.h"


void CSortName::process(CFileGroup & files) {
    std::sort(files.getFiles().begin(), files.getFiles().end(), [](const std::pair<std::unique_ptr<CAbstractFile>, bool> & f1, const std::pair<std::unique_ptr<CAbstractFile>, bool> & f2)
        {
            return f1.first->clone() -> getName() < f2.first->clone() -> getName();
        });
}
