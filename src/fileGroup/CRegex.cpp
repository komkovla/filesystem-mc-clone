#include "CRegex.h"
void CRegex::process(CFileGroup &files) {
    std::smatch sm; // string match
    for(auto & it : files.getFiles()){
        it.second = std::regex_search(it.first->getName(), sm, exp);
    }
}

CRegex::CRegex(const std::string &exp) {
    try{
        this->exp = exp;
    }
    catch (std::regex_error & error) {

    }
}
