#pragma once


#include "CAbstractSort.h"
#include <regex>
/**
 * Setting file selection flag up in the group according to the regular expression
 */
class CRegex{
public:
    /**
     * Creates with regular expression exp
     * @param exp
     */
    CRegex(const std::string & exp);
    /**
     * Setting selection flag up in the group of 'files'
     */
    void process(CFileGroup &files);
protected:
    std::regex exp;
};



