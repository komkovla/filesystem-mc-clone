#pragma once
#include <filesystem>
namespace fs = std::filesystem;

/**
 *
 *File manager application that can browse through filesystem,
 *complete operations like copy, paste, move and delete, on single files or on group of files
 */
struct CFileManager {
    /**
     * Run File Manager app
     */
     ~CFileManager() = default;
    void run();

};



