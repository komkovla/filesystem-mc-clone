#include "CFileManager.h"

//group
#include "../fileGroup/CFileGroup.h"
#include "../fileGroup/CSortSize.h"


//operace
#include "../operation/COpen.h"
#include "../operation/CClose.h"
#include "../operation/CPaste.h"
#include "../operation/CDelete.h"
#include "../operation/CCopy.h"
#include "../operation/CMove.h"

//interface
#include "../UI/CTerminal.h"
#include "../operation/CList.h"
#include "../operation/CSort.h"
#include "../operation/CExit.h"
#include "../operation/CCreate.h"



//system
#include <unistd.h>
#include <pwd.h>
#include <memory>
#include <map>


#define ERR_IS_A_DIRECTORY 1
#define ERR_WRONG_NUMBER 2

//forward declaration
class COperation;


/**
 * Read active user's home directory
 * @return Home directory path
 */
fs::path getHomeLoc(){
    struct  passwd *pw = getpwuid(getuid()); //get userinfo
    return pw ->pw_dir;
}
/**
 * set map of operations that will be active in application
 * @param operationMap - map to set the operations
 */
void setMap(std::map<int, std::unique_ptr<COperation>> & operationMap){
    operationMap[1] = std::make_unique<CList>("List");
    operationMap[2] = std::make_unique<CSort>("Sort");
    operationMap[3] = std::make_unique<COpen>("Open");
    operationMap[4] = std::make_unique<CClose>("Close folder");
    operationMap[5] = std::make_unique<CCopy>("Copy");
    operationMap[6] = std::make_unique<CPaste>("Paste");
    operationMap[7] = std::make_unique<CMove>("Move");
    operationMap[8] = std::make_unique<CDelete>("Delete");
    operationMap[9] = std::make_unique<CExit>("Exit program");
    operationMap[10] = std::make_unique<CCreate>("Create File");
}

void CFileManager::run() {
    std::unique_ptr<CInterface> interface = std::make_unique<CTerminal>();
    CFileGroup buffer;
    CFileGroup currentGroup(getHomeLoc());
    std::map<int, std::unique_ptr<COperation>> operationMap;
    setMap(operationMap);
    int operationIndex;
    while(true) {
        operationIndex = interface->menu(operationMap, currentGroup);
        if(operationIndex)
            if(!operationMap[operationIndex]->process(currentGroup, interface, buffer)) return;
    }
}