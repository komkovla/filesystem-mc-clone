#pragma once
#include <iostream>
#include <map>

#include "../fileGroup/CFileGroup.h"
class CAbstractFile;
class CAbstractSort;
class COperation;
/**
 * Parent class for all interface types in program
 * all user interaction are implemented in interface types
 */
struct CInterface {
    /**
     * Implicit constructor prepare interface
     */
    CInterface() = default;
    /**
     * Destructor deallocate all memory that was allocated by interface
     */
    virtual ~CInterface() =default;
    /**
     * print out all 'files' in the group
     * @param files- current files in path
     */
    virtual void print(const CFileGroup &files) const  = 0;
    /**
     * Prints any string
     * @param stringToPrint
     */
    virtual std::ostream & print(const std::string & stringToPrint) const  = 0;
    virtual int menu(const std::map<int, std::unique_ptr<COperation>> &operations, const CFileGroup &group) const = 0;
    /**
     * Choose one file menu for files from 'group'
     * @param files- current files in path
     * @return index of file chosen by user
     */
    virtual int chooseFileMenu(const CFileGroup &files) const  = 0;
    /**
     * Choose one or more files from 'files', sets filter flag for files chosen
     * @param files - current files in path
     * @return true - success, false - cancel or wrong file index
     */
    virtual bool fileOperationMenu(CFileGroup &files) const = 0;
    /**
     * Print and choose sorting option from 'sortMap'
     * @param sortMap - all sorting methods available
     * @return ptr on chosen sort class
     */
    virtual int sortMenu(const std::map<int, const std::unique_ptr<CAbstractSort>> &sortMap) const = 0;
    /**
     * Print out exception 'e' explanation
     * @param e
     */
    virtual void printException(const std::exception &e) const = 0;
    /**
     * Choose and check if path chosen by user is valid
     * @param group - current files in path
     * @return pair<path(chosen path), bool(is Valid)>
     */
    virtual std::pair<fs::path, bool> choosePath(const CFileGroup &group) const = 0;
    /**
     * create file, choose type of the file, check if valid number
     * @param group - current file group
     * @param fileTypes - all available filetypes
     * @return index of type chosen (0 on wrong number)
     */
     virtual int chooseFileType(const CFileGroup &group, const std::map<int, std::unique_ptr<CAbstractFile>> &fileTypes) const = 0;
     /**
      * Read line with spaces
      * @param group - current files in path
      * @return filename string
      */
     virtual std::string readFileName(const CFileGroup &group) const = 0;
};



