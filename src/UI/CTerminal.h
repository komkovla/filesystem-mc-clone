
#pragma once
#include <vector>

#include <filesystem>
#include <map>
#include "CInterface.h"

namespace fs = std::filesystem;

class CTerminal : public CInterface{
public:
    CTerminal() = default;
    ~CTerminal() = default;
    void                        print               (const CFileGroup & files)                                                  const override;
    std::ostream &              print               (const std::string & stringToPrint)                                         const override;
    int                         menu                (const std::map<int, std::unique_ptr<COperation>> &operations, const CFileGroup &group) const override;
    int                         chooseFileMenu      (const CFileGroup &files)                                                   const override;
    bool                        fileOperationMenu   (CFileGroup &files)                                                         const override;
    void                        printException      (const std::exception &e)                                                         const override;
    int                         sortMenu            (const std::map<int,const std::unique_ptr<CAbstractSort>> &sortMap)              const override;
    std::pair<fs::path, bool>   choosePath          (const CFileGroup &group)                                                         const override;
    virtual int                 chooseFileType      (const CFileGroup &group, const std::map<int, std::unique_ptr<CAbstractFile>> &fileTypes)const override;
    virtual std::string         readFileName        (const CFileGroup &group) const override;
private:
};



