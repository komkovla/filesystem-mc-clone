#include "CTerminal.h"
#include "../fileGroup/CRegex.h"
#include <iostream>
#include <map>
#include "../operation/COperation.h"

void CTerminal::print(const CFileGroup & files) const{
    for(const auto & it : files.getFiles()){
        std::cout << *it.first << std::endl;
    }
}
/**
 * Read command index from cin
 * @return command index
 */
int readCommand() {
    int command;
    std::cin >> command;
    if(std::cin.fail()){
        std::cin.clear();
        std::cin.ignore(256,'\n');
        return -1;
    }
    return command;
}

/**
 * Print wrong command
 */
void wrongCommand() {
    std::cout << "Wrong command" << std::endl;
}
/**
 * calvulate if 'val' is in interval include min, max
 * @param val value to check
 * @param min minimum value
 * @param max maximum value
 * @return
 */
bool isInInterval(int val, int min, int max){
    return val >= min && val <= max;
}

int CTerminal::menu(const std::map<int, std::unique_ptr<COperation>> &operations, const CFileGroup &group) const { // * operace
    std::cout << "Current path: " << group.getPath().lexically_normal().c_str() << std::endl;
    for (auto it = operations.begin(); it != operations.end() ; it++)
            std::cout << it->first << ") " << it->second -> getName() << std::endl;
    int index = readCommand();
    if(operations.find(index) != operations.end())
        return index;
    else {
        wrongCommand();
        return 0;
    }
}
void CTerminal::printException(const std::exception &e) const {
    std::cout << "Exception : " << e.what() <<  std::endl;
}
/**
 * read one word from cin
 * @return parsed word
 */
std::string readWord(){
    std::string res;
    std::cin >> res;
    return res;
}
/**
 * Print 'files' with indexes starting with 1
 * @param files
 */
void showFilesMenu(const CFileGroup & files) {
    for(size_t i = 0; i < files.getFiles().size(); i++){
        std::cout << i+1 << ") " << *files.getFiles()[i].first << std::endl;
    }
}

bool CTerminal::fileOperationMenu(CFileGroup &files) const {
    std::cout << "0) Choose more by regex" << std::endl;
    showFilesMenu(files);
    int index = readCommand() - 1;
    files.clearSelect();
    if(index < 0 || (size_t)index > files.getFiles().size() - 1) {
        if (index == -1) {
            std::cout << "write regex: ";
            CRegex regex(readWord());
                regex.process(files);
            std::cout << "Files chosen: " << std::endl;
            for (auto &it : files.getFiles()) {
                if (it.second) {

                    std::cout << *it.first << std::endl;
                }
            }
            std::cout << "proceed?" << std::endl
                      << "1) yes" << std::endl
                      << "2) no" << std::endl;
            int command = readCommand();
            return command == 1;
        }
        else {
            wrongCommand();
            return false;
        }
    }
    files.getFiles()[index].second = true;
    return true;

}

int CTerminal::chooseFileMenu(const CFileGroup &files) const {
    showFilesMenu(files);
    int index = readCommand() - 1;
    if(index < 0 || (size_t)index > files.getFiles().size() - 1) {
        wrongCommand();
        return -1;
    }
    else{
        return index;
    }
}

int CTerminal::sortMenu(const std::map<int, const std::unique_ptr<CAbstractSort>> &sortMap) const {
    for (auto it = sortMap.begin(); it != sortMap.end() ; it++)
        std::cout << it->first <<") " << it->second-> getName() << std::endl;
    int index = readCommand();
    if(sortMap.find(index) != sortMap.end())
        return index;
    else {
        wrongCommand();
        return 0;
    }
}



std::pair<fs::path, bool> CTerminal::choosePath(const CFileGroup & group) const{
    std::cout << "path to : "
              << group.getPath().lexically_normal().c_str() << '/';
    fs::path newPath = group.getPath() / readWord();
    if(exists(status(newPath))) return std::make_pair(newPath,true);
    else{
        wrongCommand();
        return std::make_pair(newPath, false);
    }
}

std::ostream & CTerminal::print(const std::string &stringToPrint) const {
    std::cout << stringToPrint;
    return std::cout;
}


int CTerminal::chooseFileType(const CFileGroup &group, const std::map<int, std::unique_ptr<CAbstractFile>> &fileTypes) const {
    int i = 1;
    for(auto & it : fileTypes){
        std::cout << i << ") " <<it.second->getPath().c_str() << std::endl;
        i++;
    }
    int command = readCommand();
    if(!isInInterval(command, 1,  (int)fileTypes.size()+1)){
        wrongCommand();
        return 0;
    }
    else return command;
}

std::string CTerminal::readFileName(const CFileGroup &group) const {
    std::cout << "Write file name: ";
    std::cin.ignore();
    std::string newfileName;
    std::getline(std::cin, newfileName, '\n');
    if(newfileName.empty()) return std::string();
    if(exists((group.getPath() / newfileName))){
        std::cout << "File exists" << std::endl;
        return std::string();
    }
    else{
        return newfileName;
    }
}


