CXX=g++
LD=g++
CXXFLAGS=  -std=c++17 -Wall -pedantic
LDFLAGS= -lstdc++fs -lncurses
BUILD_DIR := .../build
SRC_DIR := .../src
FILE_DIR := $(SRC_DIR)/file
FILE_FILES := $(wildcard $(FILE_DIR)/*.cpp)


all: compile doc

compile: komkovla

komkovla: build/main.o build/file/CAbstractFile.o build/file/CDirectory.o build/file/CFile.o build/file/CSymbolicLink.o build/fileGroup/CFileGroup.o build/fileGroup/CRegex.o build/fileGroup/CSortName.o build/fileGroup/CSortSize.o build/UI/CTerminal.o build/fileManager/CFileManager.o build/operation/CClose.o build/operation/CCopy.o build/operation/CDelete.o build/operation/CExit.o build/operation/CList.o build/operation/CMove.o build/operation/COpen.o build/operation/CPaste.o build/operation/CSort.o build/operation/CCreate.o
	$(LD) $(CXXFLAGS) -o $@ $^ $(LDFLAGS)

test_all : test1 test2 test3 test4 test5

test1:
	valgrind ./komkovla <examples/in_001.txt >examples/out_001.txt
test2:
	valgrind ./komkovla <examples/in_002.txt >examples/out_002.txt
test3:
	valgrind ./komkovla <examples/in_003.txt >examples/out_003.txt
test4:
	valgrind ./komkovla <examples/in_004.txt >examples/out_004.txt
test5:
	valgrind ./komkovla <examples/in_005.txt >examples/out_005.txt
test6:
	valgrind ./komkovla <examples/in_006.txt >examples/out_006.txt
test7:
	valgrind ./komkovla <examples/in_007.txt >examples/out_007.txt



run:
	./komkovla
mem_check_run:
	valgrind --leak-check=full --gen-suppressions=all --leak-check=full --leak-resolution=med --track-origins=yes ./komkovla
build/main.o:src/main.cpp
	mkdir -p "build"
	$(CXX) $(CXXFLAGS) -c -o $@ $<

file: build/file/CAbstractFile.o build/file/CDirectory.o build/file/CFile.o build/file/CSymbolicLink.o

build/file/CAbstractFile.o:src/file/CAbstractFile.cpp src/file/CAbstractFile.h
	mkdir -p build/file
	$(CXX) $(CXXFLAGS) -c -o $@ $<
#build/file/%.o: src/file/%.cpp src/file/%.h
#	$(CXX) $(CXXFLAGS) -c -o $@
build/file/CFile.o:src/file/CFile.cpp src/file/CFile.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
build/file/CSymbolicLink.o:src/file/CSymbolicLink.cpp src/file/CSymbolicLink.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
build/file/CDirectory.o:src/file/CDirectory.cpp src/file/CDirectory.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<


fileGroup: build/fileGroup/CFileGroup.o build/fileGroup/CRegex.o build/fileGroup/CSortName.o build/fileGroup/CSortSize.o

build/fileGroup/CFileGroup.o:src/fileGroup/CFileGroup.cpp src/fileGroup/CFileGroup.h
	mkdir -p build/fileGroup
	$(CXX) $(CXXFLAGS) -c -o $@ $<
build/fileGroup/CRegex.o:src/fileGroup/CRegex.cpp src/fileGroup/CRegex.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
build/fileGroup/CSortName.o:src/fileGroup/CSortName.cpp src/fileGroup/CSortName.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
build/fileGroup/CSortSize.o:src/fileGroup/CSortSize.cpp src/fileGroup/CSortSize.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

UI: build/UI/CGraphic.o build/UI/CTerminal.o
build/UI/CTerminal.o:src/UI/CTerminal.cpp src/UI/CTerminal.h
	mkdir -p build/UI
	$(CXX) $(CXXFLAGS) -c -o $@ $<

fileManager: build/fileManager/CFileManager.o
build/fileManager/CFileManager.o:src/fileManager/CFileManager.cpp src/fileManager/CFileManager.h
	mkdir -p build/fileManager
	$(CXX) $(CXXFLAGS) -c -o $@ $<

operation: build/operation/CClose.o build/operation/CCopy.o build/operation/CDelete.o build/operation/CExit.o build/operation/CList.o build/operation/CMove.o build/operation/COpen.o build/operation/CPaste.o build/operation/CSort.o build/operation/CChangeInterface.o build/operation/CCreate.o
build/operation/CClose.o:src/operation/CClose.cpp src/operation/CClose.h
	mkdir -p build/operation
	$(CXX) $(CXXFLAGS) -c -o $@ $<
build/operation/CCopy.o:src/operation/CCopy.cpp src/operation/CCopy.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
build/operation/CDelete.o:src/operation/CDelete.cpp src/operation/CDelete.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
build/operation/CExit.o:src/operation/CExit.cpp src/operation/CExit.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
build/operation/CList.o:src/operation/CList.cpp src/operation/CList.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
build/operation/CMove.o:src/operation/CMove.cpp src/operation/CMove.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
build/operation/COpen.o:src/operation/COpen.cpp src/operation/COpen.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
build/operation/CPaste.o:src/operation/CPaste.cpp src/operation/CPaste.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
build/operation/CSort.o:src/operation/CSort.cpp src/operation/CSort.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<
build/operation/CCreate.o:src/operation/CCreate.cpp src/operation/CCreate.h
	$(CXX) $(CXXFLAGS) -c -o $@ $<

deps:
	$(CXX) -MM build/file/*.cpp build/UI/*.cpp build/fileGroup/*.cpp build/fileManager/*.cpp build/operationmake/*.cpp > Makefile.build

-include Makefile.d

clean:
	rm -r -f build komkovla doc examples/out_*.txt

doc:
	doxygen

.PHONY: doc